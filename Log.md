<!-- vscode-markdown-toc -->
* 1. [设备升级](#)
	* 1.1. [下载升级包进度](#-1)
	* 1.2. [下载成功，比对md5](#md5)
	* 1.3. [下载失败](#-1)
* 2. [睡眠管理](#-1)
	* 2.1. [养老模式](#-1)
		* 2.1.1. [开启监护](#-1)
		* 2.1.2. [开启监测](#-1)
		* 2.1.3. [结束监测](#-1)
	* 2.2. [非养老模式](#-1)
		* 2.2.1. [开启监测](#-1)
		* 2.2.2. [结束监测](#-1)
* 3. [设备管理](#-1)
	* 3.1. [固件启动](#-1)
	* 3.2. [用户设置的时区](#-1)
	* 3.3. [设置设备时间](#-1)
	* 3.4. [打开呼吸波开关](#-1)
	* 3.5. [报告中呼吸波字段](#-1)
	* 3.6. [算法输出](#-1)
		* 3.6.1. [有人无人状态](#-1)
		* 3.6.2. [心率](#-1)
		* 3.6.3. [呼吸率](#-1)
	* 3.7. [在床状态字段](#-1)
	* 3.8. [上传上床状态](#-1)
	* 3.9. [控制算法实时数据的频率](#-1)
	* 3.10. [固件重启设备](#-1)
	* 3.11. [睡眠监测初始化完成](#-1)
	* 3.12. [睡眠的原始数据bin](#bin)
* 4. [配网](#-1)
	* 4.1. [接受单条声音信息](#-1)
	* 4.2. [接受到完整的声音信息](#-1)
	* 4.3. [设备配网过程](#-1)
* 5. [网络](#-1)
	* 5.1. [查询接口请求](#-1)
	* 5.2. [查询接口请求详细信息](#-1)
	* 5.3. [连接的网络信息](#-1)
		* 5.3.1. [网络类型](#-1)
		* 5.3.2. [信号强度](#-1)
	* 5.4. [网络是否连通](#-1)
* 6. [OQC](#OQC)
	* 6.1. [手动设置OQC监测成功](#OQC-1)
* 7. [用户管理](#-1)
	* 7.1. [查询海外设备用户类型](#-1)
* 8. [戒指管理](#-1)
	* 8.1. [绑定戒指](#-1)
		* 8.1.1. [长按操作](#-1)
		* 8.1.2. [戒指绑定](#-1)
	* 8.2. [戒指信息](#-1)
		* 8.2.1. [戒指广播包数据](#-1)
		* 8.2.2. [戒指电量](#-1)
		* 8.2.3. [戒指连接状态](#-1)
	* 8.3. [戒指数据](#-1)
		* 8.3.1. [收取戒指数据过程](#-1)
		* 8.3.2. [戒指数据状态](#-1)
		* 8.3.3. [打开戒指监测](#-1)
		* 8.3.4. [实时数据](#-1)
	* 8.4. [扫描戒指](#-1)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name=''></a>设备升级

###  1.1. <a name='-1'></a>下载升级包进度

```
[ControlManager] process size is
```

###  1.2. <a name='md5'></a>下载成功，比对md5

```
[ControlManager] download rom md5
[ControlManager] server rom md5
```

###  1.3. <a name='-1'></a>下载失败

```
[ControlManager] update error
```

##  2. <a name='-1'></a>睡眠管理

###  2.1. <a name='-1'></a>养老模式

####  2.1.1. <a name='-1'></a>开启监护
```
[SleepManager] start sleep id
```

####  2.1.2. <a name='-1'></a>开启监测
```
[SleepManager] open sp

[SleepManager] open fixed time data result:
```

####  2.1.3. <a name='-1'></a>结束监测
```
[SleepManager] close fixed time data result:
```

###  2.2. <a name='-1'></a>非养老模式

####  2.2.1. <a name='-1'></a>开启监测
```
[SleepManager] start sleep id
[SleepManager] open fixed time data result:
```

####  2.2.2. <a name='-1'></a>结束监测
```
[SleepManager] close fixed time data result:
```

##  3. <a name='-1'></a>设备管理

###  3.1. <a name='-1'></a>固件启动

关键字：MainService

```
MainService：sn:S01A1803000041,alg version:1.4.191024,app version:2.4.9002,rom version:2.0.6,debug mode:false,time:1578497527659,data available size:5939847168,data free size:5956624384,data total size:6163398656,timeInfo:Hour:15, Minute:32, Second:7,TimeZone:GMT+00:00,bootTime:1578497464357,TimeZone id:GMT+00:00
```

	设备启动时间：bootTime

###  3.2. <a name='-1'></a>用户设置的时区

```
[AlarmConfig]Set time Zone
```

###  3.3. <a name='-1'></a>设置设备时间
```
[SetRTCReceiver]rtc time:
```

###  3.4. <a name='-1'></a>打开呼吸波开关

```
Device表中waveDataUpload字段
```

###  3.5. <a name='-1'></a>报告中呼吸波字段

```
Reports表中waveFileId字段
```

###  3.6. <a name='-1'></a>算法输出

####  3.6.1. <a name='-1'></a>有人无人状态

```
inBedDataBack
```

介绍：<br/>
0：有人<br/>
1：无人<br/>

####  3.6.2. <a name='-1'></a>心率

```
sendHeartRateDataBack
```

####  3.6.3. <a name='-1'></a>呼吸率

```
sendBreathRateDate
```

###  3.7. <a name='-1'></a>在床状态字段

```
inBed
```

介绍：<br/>
true：有人<br/>
false：无人

###  3.8. <a name='-1'></a>上传上床状态

```
updateDeviceInfo
```

###  3.9. <a name='-1'></a>控制算法实时数据的频率

```
processInterval
```

###  3.10. <a name='-1'></a>固件重启设备

```
reboot
```

###  3.11. <a name='-1'></a>睡眠监测初始化完成

```
init time success
```

###  3.12. <a name='bin'></a>睡眠的原始数据bin

拿最原始的bin文件：找到报告id-找到对应的fileid进入file找到url字段进行下载



##  4. <a name='-1'></a>配网

###  4.1. <a name='-1'></a>接受单条声音信息

```
[MyVoiceRecognition]rcv msg type
```

###  4.2. <a name='-1'></a>接受到完整的声音信息

```
[SinVoiceManager]uuid:
```

###  4.3. <a name='-1'></a>设备配网过程

关键字：WifiConfigHelper



##  5. <a name='-1'></a>网络

###  5.1. <a name='-1'></a>查询接口请求

```
https
```

###  5.2. <a name='-1'></a>查询接口请求详细信息

```
OkHttp
```

###  5.3. <a name='-1'></a>连接的网络信息

####  5.3.1. <a name='-1'></a>网络类型

```
networkType
```

0：数据流量，1：WIFI

####  5.3.2. <a name='-1'></a>信号强度

```
mobileRssi
wifiRssi
```

###  5.4. <a name='-1'></a>网络是否连通

```
adb shell / ping baidu.com
```



##  6. <a name='OQC'></a>OQC

###  6.1. <a name='OQC-1'></a>手动设置OQC监测成功

```
isPass 设置为1
```



##  7. <a name='-1'></a>用户管理

###  7.1. <a name='-1'></a>查询海外设备用户类型

```
patients表中的userInMegaHealthNew -start with-邮箱
```



##  8. <a name='-1'></a>戒指管理

###  8.1. <a name='-1'></a>绑定戒指

####  8.1.1. <a name='-1'></a>长按操作

```
[MainCtrlView] Long press
[MainCtrlView] bind ring		
```

####  8.1.2. <a name='-1'></a>戒指绑定

```
BaseBluetoothBoundManager
onPairData
```

onPairData值：<br/>
2：晃动戒指<br/>
3：电量过低

###  8.2. <a name='-1'></a>戒指信息

####  8.2.1. <a name='-1'></a>戒指广播包数据

```
MegaAdvOnly{   
```

Spo：血氧<br/>
pr：脉率<br/>
gesture：手势<br/>
stage：睡眠状态：醒着的，睡着的，无效

####  8.2.2. <a name='-1'></a>戒指电量

```
[BlePairManager] by
```

by:低于50没电，ps:是否充电，mr;是否工作

####  8.2.3. <a name='-1'></a>戒指连接状态

戒指断开

```
[RingConnectorImpl] onDisconnect
```



###  8.3. <a name='-1'></a>戒指数据

####  8.3.1. <a name='-1'></a>收取戒指数据过程

戒指正在收集数据：MRDataCollect
收集戒指数据成功：SaveAllSpoData
戒指收取数据大小：Alldata

####  8.3.2. <a name='-1'></a>戒指数据状态

```
Ringstatus
```

默认，0：已开启监测，1：等待收取数据，2：收取完成

####  8.3.3. <a name='-1'></a>打开戒指监测

```
startMonitorSuccess
```

####  8.3.4. <a name='-1'></a>实时数据

双戒指版本：
```
MegaV2LiveSpoMonitor
```
spo：血氧
pr：心率
status：数据状态：0：有效值，1：准备中，2：离手


###  8.4. <a name='-1'></a>扫描戒指

```
BluetoothScanManager
MegaRingScanManager
```


